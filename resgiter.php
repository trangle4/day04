
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="edit.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    
</head>

<?php
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        //Phất cờ
        $error = array(); // Chưa có lỗi

        // Kiểm tra lỗi để trống username
        if (empty($_POST['fullname'])) { // nếu fullname rỗng 
            $error['fullname'] = "Hãy nhập tên.";
        } else {
                $fullname = $_POST['fullname'];
        }

        // Kiểm tra lỗi để trống giới tính
        if (empty($_POST['gender1'])) { // để trống giới tính
            $error['gender1'] = "Hãy chọn giới tính.";
        } else {
                $gender1 = $_POST['gender1'];
        }

        //kiểm tra lỗi để trống Khoa
        if (empty($_POST['select_khoa'])) { // để trống khoa
            $error['select_khoa'] = "Hãy chọn khoa.";
        } else {
                $select_khoa = $_POST['select_khoa'];
        }

        //kiểm tra lỗi để trống ngày sinh
       
        if (empty($_POST["birthday"])) {
            $error['birthday'] = "Hãy nhập ngày sinh.";
          } else if (!validateBirthday($_POST["birthday"])) {
            $error['birthday'] = "Hãy nhập ngày sinh đúng định dạng.";
          } 
          else {
            $birthday = $_POST["birthday"];
          }



    }

    // Hàm kiểm tra ngày sinh nhập không đúng định dạng
    function validateBirthday($birthday){
        $birthdays  = explode('/', $birthday);
        if (count($birthdays) == 3) {
          return checkdate($birthdays[1], $birthdays[0], $birthdays[2]);
        }
        return false;
      }
    

?>

<body>

<form action="" method="POST">

<div class="item_header">
    
    <!-- Hiển thị lỗi  -->
    <?php
        if(isset($error['fullname'])){
            ?>
                <span class="error"><?php echo $error['fullname']; ?> <br> </span>
            <?php
        }

        // Hiển thị lỗi của gender
        if(isset($error['gender1'])){
            ?>
                <span class="error"><?php echo $error['gender1'];?> <br> </span>
            <?php
        }

        // Hiển thị lỗi chưa chọn khoa
        if(isset($error['select_khoa'])){
            ?>
                <span class="error"><?php echo $error['select_khoa'];?><br> </span>
            <?php
        }

        // Hiển thị lỗi ngày sinh
        if(isset($error['birthday'])){
            ?>
                <span class="error"><?php echo $error['birthday'];?><br> </span>

            <?php
        }

    ?>

    <div class="header">

        <!-- div chọn họ và tên -->
        <div class="item">

            <div class="item_left">
                Họ và tên
                <span class="comment">*</span>
            </div>

            <input  id ="name" name="fullname" class="item_right" type="text" value="<?php if(isset($fullname)) echo $fullname ; ?>">
            
        </div>

        <!--  div chọn giới tính  -->
        <div class="item">

            <div class="item_left">
                Giới tính
                <span class="comment">*</span>
            </div>

            <div class="gender">
                <?php
                    $gender = array (
                        0 => "Nam",
                        1 => "Nữ"

                    );
                    for ($i=0; $i<count($gender); $i ++){

                        // danh sách hiển thị ở dạng laybel
                        // giới tính chỉ cho phép chọn 1 thì 2 thẻ iput cùng name 
                        echo " <laybel>
                            <input type='radio' class='phai' name='gender1'> 
                            $gender[$i]
                            </laybel>";
                    }  
                    
                ?> 
            </div>
            <!-- dùng mang đe luu thông tin giới tính, dùng for đê hien thị thông tin giới tính  --> 
        </div>


        <!--  div phân khoa -->
        <div class="item">

            <div class="item_left">
                Phân khoa
                <span class="comment">*</span>
            </div>

            <div class="select">

                <select class="drop" name ='select_khoa'>
                <!-- <option value="fiat"></option>
                <option value="volvo">Khoa học máy tính</option>
                <option value="saab">Khoa học dữ liệu</option> -->
                

                    <?php
                        $phankhoa = array(
                            "MTA" => "Khoa học máy tính",
                            "KDL" => "Khoa học dữ liệu"

                        );
                        echo "<option value='' class='chon1'></option>"; //du liệu để trống

                        foreach($phankhoa as $class => $val) { // dùng foreach để lấy dữ liệu từ mảng
                            echo "<option value='$class' class='chon1 ' >$val</option>"; 
                            
                        }

                        

                        
                    ?>
                    
                </select>

                <?php if (isset($select_khoa)) echo  $class;  ?> 
                
                
              </div>
            
        </div>

        <!-- div chọn ngày sinh -->
        <div class="item">

            <div class="item_left">
                Ngày sinh
                <span class="comment">*</span>
            </div>

            <div class="select">
                <div class="input-group date" id="datepicker">
                    <input type="text" class="iterm_birthday1 item_right drop " name="birthday" placeholder="dd/mm/yyyy">
                    <span class="input-group-append">
                    </span>
                </div>
            </div>
        </div>


        <!-- div chọn địa chỉ -->
        <div class="item">

            <div class="item_left">
                Địa chỉ
            </div>

            <input  id ="address" class="item_right" type="text">
        </div>

        <!-- button đăng ký -->
        <button class="button_submit" name="submit">
            Đăng ký
        </button>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#datepicker').datepicker({
                format: 'dd/mm/yyyy'
            });
        });
    </script>

</div>
</form>
</body>
</html>
